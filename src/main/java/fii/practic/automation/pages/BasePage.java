package fii.practic.automation.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor javascriptExecutor;
    protected static final Logger logger = LogManager.getRootLogger();


    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        this.javascriptExecutor = (JavascriptExecutor) driver;
    }

    public void waitTime(int timeToWaitInMilliseconds) {
        try {
            Thread.sleep(timeToWaitInMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    public String getPageTitle(){
        return driver.getTitle();
    }

}
