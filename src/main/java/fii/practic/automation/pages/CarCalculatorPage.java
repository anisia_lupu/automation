package fii.practic.automation.pages;

import fii.practic.automation.utilities.PageLoading;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CarCalculatorPage extends BasePage implements PageLoading {

    private By okButton = By.xpath("//a[contains(text(), 'OK')]");
    private By distanceInputBox = By.id("car_calculator_km");
    private By fuelConsumptionInputBox = By.id("car_calculator_fuel_consumption");
    private By calculateButton = By.xpath("//button[contains(@class, 'btn btn-primary')]");
    private By displayedAmount = By.id("co2_amount");
    private By tripDetailsText = By.xpath("//div[@id='car_calc_details']/p");

    public CarCalculatorPage(WebDriver driver) {
        super(driver);
    }

    public void clickOkForCookies(){
        WebElement button = wait.until(ExpectedConditions.presenceOfElementLocated(okButton));
        button.click();
        waitTime(1000);
    }

    public void insertDistanceTraveled(String distance){
        WebElement inputBox = driver.findElement(distanceInputBox);
        inputBox.sendKeys(distance);
    }

    public void selectFuelType(String fuelType){
        WebElement fuelTypeCheckBox = driver.findElement(By.id("car_calculator_fuel_type_" + fuelType));
        fuelTypeCheckBox.click();
    }

    public void insertFuelConsumption(String consumption){
        WebElement fuelConsumption_inputBox = driver.findElement(fuelConsumptionInputBox);
        fuelConsumption_inputBox.sendKeys(consumption);
    }

    public void clickOnCalculateButton(){
        driver.findElement(calculateButton).click();
    }

    public String getDisplayedConsumption(){
        String text = driver.findElement(displayedAmount).getText();
        String consumption = text.substring(text.lastIndexOf(":") + 1).trim();

        logger.info("Co2 consumption: " + consumption);

        return consumption;
    }

    public String getTripDetails(){
        String text = driver.findElement(tripDetailsText).getText().trim();
        logger.info("Trip details: " + text);
        return text;
    }

    @Override
    public void waitToLoad() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("new_car_calculator")));

    }
}
