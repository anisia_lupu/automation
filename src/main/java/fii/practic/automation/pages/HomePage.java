package fii.practic.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HomePage extends BasePage {

    private By cookieButton = By.id("cookieAction");
    private By carIcon = By.xpath("//a[@class='Car flex-item']");
    private By houseHoldIcon = By.xpath("//a[@class='Household flex-item']");


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnAgreeButton(){
        WebElement button = driver.findElement(cookieButton);
        button.click();
        waitTime(1000);
    }

    public CarCalculatorPage clickOnCarIcon(){
        WebElement icon =  wait.until(ExpectedConditions.elementToBeClickable(carIcon));
        icon.click();
        return new CarCalculatorPage(getDriver());
    }

    public HouseHoldCalculatorPage clickOnHouseholdIcon(){
        WebElement icon =  wait.until(ExpectedConditions.elementToBeClickable(houseHoldIcon));
        icon.click();
        return new HouseHoldCalculatorPage(getDriver());
    }


}
