package fii.practic.automation.pages;

import fii.practic.automation.utilities.PageLoading;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class HouseHoldCalculatorPage extends BasePage implements PageLoading {

    private By okButton = By.xpath("//a[contains(text(), 'OK')]");
     private By formId = By.id("new_household_calculator");

    public HouseHoldCalculatorPage(WebDriver driver) {
        super(driver);
        waitToLoad();
    }

    public void clickOkForCookies(){
        WebElement button = wait.until(ExpectedConditions.presenceOfElementLocated(okButton));
        button.click();
        waitTime(1000);
    }

    public void selectFuelType(String fuelType){
       //TODO implement me!
    }

    public void insertFuelAmount(String amount){
        //TODO implement me!
    }

    public void chooseFuelUnit(String fuelUnit){
        //TODO implement me!
    }

    public void insertEnergyConsumptionValue(String value){
        //TODO implement me!
    }

    public void chooseGreenEnergy(){
        //TODO implement me!
    }

    public void selectCountry(String country){
        //TODO implement me!
    }

    public void clickOnCalculateButton(){
        //TODO implement me!
    }

    public String getDisplayedConsumption(){
        String text = driver.findElement(By.id("co2_amount")).getText();
        String consumption = text.substring(text.lastIndexOf(":") + 1).trim();
        logger.info("Co2 consumption: " + consumption);
        return consumption;
    }

    @Override
    public void waitToLoad() {
        wait.until(ExpectedConditions.presenceOfElementLocated(formId));
    }
}
