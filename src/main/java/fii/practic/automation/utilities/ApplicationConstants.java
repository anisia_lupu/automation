package fii.practic.automation.utilities;

public interface ApplicationConstants {

    String PROPS_FILE = "application.properties";

    String CHROME_WEBDRIVER_PATH_PROP_ENTRY = "webdriver.chrome.driver.path";

    String IE_WEBDRIVER_PATH_PROP_ENTRY = "webdriver.ie.driver.path";

    String FIREFOX_WEBDRIVER_PATH_PROP_ENTRY = "webdriver.gecko.driver.path";

    String APP_URL_PROP_ENTRY = "application.url";

    String DRIVER_NAME = "driver.name";

}
