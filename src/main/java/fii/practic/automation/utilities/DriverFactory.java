package fii.practic.automation.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import static fii.practic.automation.utilities.ApplicationConstants.*;
import static fii.practic.automation.utilities.PropertyContainer.*;

public class DriverFactory {

    private Map<String, Supplier<WebDriver>> drivers;

    public WebDriver getDriver() {
       return drivers.get(getProperty(DRIVER_NAME)).get();
    }

    public static DriverFactory instance(){
        DriverFactory result = new DriverFactory();
        result.initDrivers();
        return result;
    }

    private void initDrivers() {
        drivers = new HashMap<>();
        drivers.put("ie", this::getIEDriver);
        drivers.put("chrome", this::getChromeDriver);
        drivers.put("firefox", this::getFirefoxDriver);
    }

    private WebDriver getChromeDriver(){
        System.setProperty("webdriver.chrome.driver", getProperty(CHROME_WEBDRIVER_PATH_PROP_ENTRY));
        WebDriver driver = new ChromeDriver();
        return driver;
    }

    private WebDriver getIEDriver(){
        System.setProperty("webdriver.ie.driver", getProperty(IE_WEBDRIVER_PATH_PROP_ENTRY));
        WebDriver driver = new InternetExplorerDriver();
        return driver;
    }

    private WebDriver getFirefoxDriver(){
        System.setProperty("webdriver.gecko.driver", getProperty(FIREFOX_WEBDRIVER_PATH_PROP_ENTRY));
        WebDriver driver = new FirefoxDriver();
        return driver;
    }


}
