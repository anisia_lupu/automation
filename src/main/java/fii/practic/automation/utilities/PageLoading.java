package fii.practic.automation.utilities;

public interface PageLoading {

    void waitToLoad();
}
