package fii.practic.automation.utilities;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class PropertyContainer {
    private static final Object mutex = new Object();
    private static volatile Config conf;

    public static String getProperty(String property) {
        if (conf == null) {
            synchronized (mutex) {
                if (conf == null) {
                    conf = ConfigFactory.load(ApplicationConstants.PROPS_FILE);
                }
            }
        }
        return conf.getString(property);

    }
}
