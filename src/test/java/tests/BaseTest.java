package tests;

import fii.practic.automation.utilities.DriverFactory;
import fii.practic.automation.utilities.PropertyContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;

import static fii.practic.automation.utilities.ApplicationConstants.APP_URL_PROP_ENTRY;

abstract public class BaseTest {

    private static WebDriver driver;
    protected static final Logger logger = LogManager.getRootLogger();

    @BeforeClass
    public static void init() {
        logger.info("Executing test suite");
    }

    @Before
    public void setUp() {
        // Create an instance of the driver
        driver = DriverFactory.instance().getDriver();

        //Navigate to a page
        driver.get(PropertyContainer.getProperty(APP_URL_PROP_ENTRY));

        //Maximize Window
        driver.manage().window().maximize();

    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Rule
    public TestRule watcher = new TestWatcher() {
        @Override
        protected void starting(Description description) {
            logger.info("Starting test: " + description.getMethodName());
        }

        @Override
        protected void finished(Description description) {
            logger.info("Test finished: " + description.getMethodName());
        }

        @Override
        protected void failed(Throwable e, Description description) {
            logger.info("Test failed: " + description.getMethodName());
            super.failed(e, description);
        }
    };

    protected WebDriver getDriver() {
        return driver;
    }
}