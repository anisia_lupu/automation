package tests;

import fii.practic.automation.pages.CarCalculatorPage;
import fii.practic.automation.pages.HomePage;
import org.junit.Assert;
import org.junit.Test;

public class CarEmissionsTest extends BaseTest {


    private HomePage homePage;
    private CarCalculatorPage carCalculatorPage;

    @Test
    public void testConsumptionCo2_Diesel(){
         homePage = new HomePage(getDriver());
         homePage.clickOnAgreeButton();

         carCalculatorPage = homePage.clickOnCarIcon();
         carCalculatorPage.clickOkForCookies();
         carCalculatorPage.insertDistanceTraveled("500");
         carCalculatorPage.selectFuelType("diesel");
         carCalculatorPage.insertFuelConsumption("7");
         carCalculatorPage.clickOnCalculateButton();

         Assert.assertEquals("0.138 t", carCalculatorPage.getDisplayedConsumption());
         Assert.assertEquals("Offset a ride of 500 km, Fuel: Diesel", carCalculatorPage.getTripDetails());
    }

    @Test
    public void testConsumptionCo2_NaturalGas(){
        homePage = new HomePage(getDriver());

        logger.info(homePage.getPageTitle());
        homePage.clickOnAgreeButton();

        carCalculatorPage = homePage.clickOnCarIcon();
        carCalculatorPage.clickOkForCookies();
        carCalculatorPage.insertDistanceTraveled("500");
        carCalculatorPage.selectFuelType("naturalgas");
        carCalculatorPage.insertFuelConsumption("7");
        carCalculatorPage.clickOnCalculateButton();

        Assert.assertEquals("0.144 t", carCalculatorPage.getDisplayedConsumption());
        Assert.assertEquals("Offset a ride of 500 km, Fuel: Natural gas", carCalculatorPage.getTripDetails());

    }

}
