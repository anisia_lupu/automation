package tests;

import fii.practic.automation.pages.HomePage;
import fii.practic.automation.pages.HouseHoldCalculatorPage;
import org.junit.Assert;
import org.junit.Test;

public class HouseHoldEmissionsTest extends BaseTest {

    private HomePage homePage;
    private HouseHoldCalculatorPage houseHoldCalculatorPage;

    @Test
    public void testHouseholdConsumption(){
        homePage = new HomePage(getDriver());
        homePage.clickOnAgreeButton();

        houseHoldCalculatorPage = homePage.clickOnHouseholdIcon();
        houseHoldCalculatorPage.clickOkForCookies();
        houseHoldCalculatorPage.selectFuelType("natural_gas");
        houseHoldCalculatorPage.insertFuelAmount("30");
        houseHoldCalculatorPage.chooseFuelUnit("m3");
        houseHoldCalculatorPage.insertEnergyConsumptionValue("2000");
        houseHoldCalculatorPage.chooseGreenEnergy();
        houseHoldCalculatorPage.selectCountry("Romania");
        houseHoldCalculatorPage.clickOnCalculateButton();

        Assert.assertEquals("0.114 t", houseHoldCalculatorPage.getDisplayedConsumption());

    }

}
